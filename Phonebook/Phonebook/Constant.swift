import Foundation

struct K {
    static let workContact = "WorkContacts"
    static let homeContact = "HomeContacts"
    static let privateContact = "PrivateContacts"
    
    static let nibName = "ContactCell"
    static let reuseIdentifier = "ReusableCell"
    
    static let nameLabelTag = 1000
    static let numberLabelTag = 1001
    static let descriptionLabelTag = 1002
    static let cellImageTag = 1003
    
    static let maleGenderImage = "male.svg"
    static let femaleGenderImage = "female.svg"
    
    static let editContactIdentifier = "EditContact"
    static let addContactIdentifier = "AddContact"
    
    static let alertTitle = "Delete Contact"
}
