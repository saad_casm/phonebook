import Foundation

class DataModel {
    
    var categories = [ContactCategory]()
    
    init() {
        loadPhonebook()
    }
    
    func documentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    func dataFilePath() -> URL {
        return documentsDirectory().appendingPathComponent("Phonebook.plist")
    }
    
    func savePhonebook() {
        let encoder = PropertyListEncoder()
        do {
            let data = try encoder.encode(categories)
            try data.write(to: dataFilePath(), options: Data.WritingOptions.atomic)
        } catch {
            print("Error Encoding: \(error.localizedDescription)")
        }
    }
    
    func loadPhonebook() {
        let path = dataFilePath()
        
        if let data = try? Data(contentsOf: path) {
            let decoder = PropertyListDecoder()
            do {
                categories = try decoder.decode([ContactCategory].self, from: data)
            } catch {
                print("Error Decoding: \(error.localizedDescription)")
            }
        }
    }
    
    func printFileName() {
        print(dataFilePath())
    }
}
