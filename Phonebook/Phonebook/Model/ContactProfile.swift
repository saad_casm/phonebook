import UIKit

class ContactProfile: NSObject, Codable {
    var name: String = ""
    var number: String = ""
    var contactDescription: String = ""
    var genderImage: String = "question.svg"
}
