import UIKit

class ContactCategory: NSObject, Codable {
    var name = ""
    var contacts = [ContactProfile]()
    
    init(name: String) {
        self.name = name
        super.init()
    }
}
