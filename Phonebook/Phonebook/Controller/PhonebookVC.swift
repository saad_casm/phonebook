import UIKit

class PhonebookVC: UIViewController {
    
    var dataModel = DataModel()
    
    @IBOutlet weak var workButton: UIButton!
    @IBOutlet weak var homeButton: UIButton!
    @IBOutlet weak var privateButton: UIButton!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        configureNavigationControllerViewWillAppear()
    }
    
    func configureNavigationControllerViewWillAppear() {
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0, green: 0.5898008943, blue: 1, alpha: 1)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
    }
    
    func configureNavigationControllerViewDidLoad() {
        navigationController?.navigationBar.prefersLargeTitles = false
        navigationItem.largeTitleDisplayMode = .never
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureNavigationControllerViewDidLoad()
        configureRoundButton()
    }
    
    func configureRoundButton() {
        workButton.layer.cornerRadius = 30
        homeButton.layer.cornerRadius = 30
        privateButton.layer.cornerRadius = 30
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == K.workContact {
            let controller = segue.destination as! ContactListVC
            controller.dataModel = dataModel
            controller.categoryNum = 0
        } else if segue.identifier == K.homeContact {
            let controller = segue.destination as! ContactListVC
            controller.dataModel = dataModel
            controller.categoryNum = 1
        } else if segue.identifier == K.privateContact {
            let controller = segue.destination as! ContactListVC
            controller.dataModel = dataModel
            controller.categoryNum = 2
        }
    }
}
