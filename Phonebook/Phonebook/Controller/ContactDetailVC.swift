import UIKit

protocol ContactDetailVCDelegate {
    func contactDetailVCDidCancel(_ controller: ContactDetailVC)
    func contactDetailVCDidFinishAdding(_ controller: ContactDetailVC, didFinishAdding contact: ContactProfile)
    func contactDetailVCDidFinishEditing(_ controller: ContactDetailVC, didFinishEditing contact: ContactProfile)
}

class ContactDetailVC: UIViewController {
    
    var gender: String = "question"
    var delegate: ContactDetailVCDelegate?
    var contactToEdit: ContactProfile?
    
    let compareGender: String = "question"
    
    @IBOutlet weak var maleImageView: UIImageView!
    @IBOutlet weak var femaleImageView: UIImageView!
    @IBOutlet weak var maleButton: UIButton!
    @IBOutlet weak var femaleButton: UIButton!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var numberTextField: UITextField!
    @IBOutlet weak var descriptionTextField: UITextField!
    @IBOutlet weak var doneBarButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureViewControllerSettings()
        configureNavigationController()
    }
    
    func configureViewControllerSettings() {
        if let contact = contactToEdit {
            title = "Edit Contact"
            nameTextField.text = contact.name
            numberTextField.text = contact.number
            descriptionTextField.text = contact.contactDescription
            
            if contact.genderImage == "male.svg" {
                gender = "male.svg"
                maleButton.isSelected = true
                maleImageView.backgroundColor = #colorLiteral(red: 0.4620226622, green: 0.8382837176, blue: 1, alpha: 1)
                maleImageView.image = UIImage(named: "male_selected.svg")
                femaleButton.isSelected = false
                femaleImageView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
                femaleImageView.image = UIImage(named: "female.svg")
            } else if contact.genderImage == "female.svg" {
                gender = "female.svg"
                maleButton.isSelected = false
                maleImageView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
                maleImageView.image = UIImage(named: "male.svg")
                femaleButton.isSelected = true
                femaleImageView.backgroundColor = #colorLiteral(red: 1, green: 0.5409764051, blue: 0.8473142982, alpha: 1)
                femaleImageView.image = UIImage(named: "female_selected.svg")
            } else {
                gender = "question.svg"
                maleButton.isSelected = false
                maleImageView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
                maleImageView.image = UIImage(named: "male.svg")
                femaleButton.isSelected = false
                femaleImageView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
                femaleImageView.image = UIImage(named: "female.svg")
            }
            
            doneBarButton.isEnabled = false
            [nameTextField, numberTextField, descriptionTextField].forEach({ $0.addTarget(self, action: #selector(editingChanged), for: .editingChanged) })
            
            addReturnBarButtonOnKeybaord()
        }
        
        maleImageView.layer.cornerRadius = 15
        femaleImageView.layer.cornerRadius = 15
        
        nameTextField.becomeFirstResponder()
        nameTextField.delegate = self
        nameTextField.tag = 0
        numberTextField.tag = 1
        descriptionTextField.tag = 2
    }
    
    func configureNavigationController() {
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.largeTitleDisplayMode = .never
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0, green: 0.5898008943, blue: 1, alpha: 1)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
    }
    
    @IBAction func selectGender(_ sender: UIButton) {
        if title == "Edit Contact" {
            doneBarButton.isEnabled = true
        }
        if sender == maleButton {
            gender = "male.svg"
            maleButton.isSelected = true
            maleImageView.backgroundColor = #colorLiteral(red: 0.4620226622, green: 0.8382837176, blue: 1, alpha: 1)
            maleImageView.image = UIImage(named: "male_selected.svg")
            femaleButton.isSelected = false
            femaleImageView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
            femaleImageView.image = UIImage(named: "female.svg")
        } else {
            gender = "female.svg"
            maleButton.isSelected = false
            maleImageView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
            maleImageView.image = UIImage(named: "male.svg")
            femaleButton.isSelected = true
            femaleImageView.backgroundColor = #colorLiteral(red: 1, green: 0.5409764051, blue: 0.8473142982, alpha: 1)
            femaleImageView.image = UIImage(named: "female_selected.svg")
        }
    }
    
    @IBAction func cancelBarButton(_ sender: UIBarButtonItem) {
        delegate!.contactDetailVCDidCancel(self)
    }
    
    @IBAction func donePressed() {
        if let contact = contactToEdit {
            contact.name = nameTextField.text!
            contact.number = numberTextField.text!
            contact.contactDescription = descriptionTextField.text!
            contact.genderImage = gender
            
            delegate?.contactDetailVCDidFinishEditing(self, didFinishEditing: contact)
        } else {
            let contact = ContactProfile()
            contact.name = nameTextField.text!
            contact.number = numberTextField.text!
            contact.contactDescription = descriptionTextField.text!
            contact.genderImage = gender
            
            delegate?.contactDetailVCDidFinishAdding(self, didFinishAdding: contact)
        }
    }
}


// MARK:- Extension UITextFieldDelegate

extension ContactDetailVC: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return false
    }
    
    @objc func configureNextButton() -> Any {
        return textFieldShouldReturn(numberTextField)
    }
    
    func addReturnBarButtonOnKeybaord() {
        
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace , target: nil, action: nil)
        
        let nextButton = UIBarButtonItem(title: "next", style: .done, target: self, action: #selector(self.configureNextButton))
        
        toolBar.setItems([flexSpace, nextButton], animated: true)
        numberTextField.inputAccessoryView = toolBar
    }
    
    @objc func editingChanged(_ textField: UITextField) {
        if textField.text?.count == 1 {
            if textField.text?.first == " " {
                textField.text = ""
                return
            }
        }
        
        guard let name = nameTextField.text, !name.isEmpty, let number = numberTextField.text, !number.isEmpty, let desc = descriptionTextField.text, !desc.isEmpty else {
            doneBarButton.isEnabled = false
            return
        }
        
        doneBarButton.isEnabled = true
    }
}
