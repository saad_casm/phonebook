import UIKit

class ContactListVC: UITableViewController, ContactDetailVCDelegate {
    
    var dataModel = DataModel()
    var categoryNum = 0
    let heightOfRow: CGFloat = 95
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = dataModel.categories[categoryNum].name
        tableView.register(UINib(nibName: K.nibName, bundle: nil), forCellReuseIdentifier: K.reuseIdentifier)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        configureNavigationController()
    }
    
    func configureNavigationController() {
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.largeTitleDisplayMode = .always
        navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.systemBlue]
        navigationController?.navigationBar.tintColor = #colorLiteral(red: 0, green: 0.5898008943, blue: 1, alpha: 1)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.systemBlue]
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataModel.categories[categoryNum].contacts.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        configureReusableCell(indexPath: indexPath)
    }
    
    func configureReusableCell(indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: K.reuseIdentifier, for: indexPath) as! ContactCell
        let contact = dataModel.categories[categoryNum].contacts[indexPath.row]
        
        let nameLabel = cell.viewWithTag(K.nameLabelTag) as! UILabel
        let numberLabel = cell.viewWithTag(K.numberLabelTag) as! UILabel
        let descriptionLabel = cell.viewWithTag(K.descriptionLabelTag) as! UILabel
        let cellImage = cell.viewWithTag(K.cellImageTag) as! UIImageView
        
        nameLabel.text = contact.name
        numberLabel.text = contact.number
        descriptionLabel.text = contact.contactDescription
        cellImage.image = UIImage(named: contact.genderImage)
        
        if contact.genderImage == K.maleGenderImage {
            cellImage.backgroundColor = #colorLiteral(red: 0.4620226622, green: 0.8382837176, blue: 1, alpha: 1)
        } else if contact.genderImage == K.femaleGenderImage {
            cellImage.backgroundColor = #colorLiteral(red: 1, green: 0.5409764051, blue: 0.8473142982, alpha: 1)
        } else {
            cellImage.backgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.performSegue(withIdentifier: K.editContactIdentifier, sender: tableView.cellForRow(at: indexPath))
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return heightOfRow
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        let contactName = dataModel.categories[categoryNum].contacts[indexPath.row].name
        
        let alertMessage = "Are you sure you want to delete \(contactName) from \(String(describing: title!))?"
        
        let alert = UIAlertController(title: K.alertTitle, message: alertMessage, preferredStyle: .alert)
        let action1 = UIAlertAction(title: "Yes", style: .default, handler: {_ in self.deleteContact(indexPath: indexPath)})
        let action2 = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
     
        alert.addAction(action1)
        alert.addAction(action2)
        present(alert, animated: true, completion: nil)
    }
    
    func deleteContact(indexPath: IndexPath) {
        dataModel.categories[categoryNum].contacts.remove(at: indexPath.row)
        
        let indexPaths = [indexPath]
        tableView.deleteRows(at: indexPaths, with: .automatic)
        dataModel.savePhonebook()
    }
    
    func configureText(for cell: UITableViewCell, with contact: ContactProfile) {
        let nameLabel = cell.viewWithTag(K.nameLabelTag) as! UILabel
        let numberLabel = cell.viewWithTag(K.numberLabelTag) as! UILabel
        let descriptionLabel = cell.viewWithTag(K.descriptionLabelTag) as! UILabel
        let cellImage = cell.viewWithTag(K.cellImageTag) as! UIImageView
        
        nameLabel.text = contact.name
        numberLabel.text = contact.number
        descriptionLabel.text = contact.contactDescription
        cellImage.image = UIImage(named: contact.genderImage)
        
        if contact.genderImage == K.maleGenderImage {
            cellImage.backgroundColor = #colorLiteral(red: 0.4620226622, green: 0.8382837176, blue: 1, alpha: 1)
        } else if contact.genderImage == K.femaleGenderImage {
            cellImage.backgroundColor = #colorLiteral(red: 1, green: 0.5409764051, blue: 0.8473142982, alpha: 1)
        } else {
            cellImage.backgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        }
    }
    
    func contactDetailVCDidCancel(_ controller: ContactDetailVC) {
        navigationController?.popViewController(animated: true)
    }
    
    func contactDetailVCDidFinishAdding(_ controller: ContactDetailVC, didFinishAdding contact: ContactProfile) {
        let newRowIndex = dataModel.categories[categoryNum].contacts.count
        dataModel.categories[categoryNum].contacts.append(contact)
        
        let indexPath = IndexPath(row: newRowIndex, section: 0)
        let indexPaths = [indexPath]
        tableView.insertRows(at: indexPaths, with: .automatic)
        dataModel.savePhonebook()
        navigationController?.popViewController(animated: true)
    }
    
    func contactDetailVCDidFinishEditing(_ controller: ContactDetailVC, didFinishEditing contact: ContactProfile) {
        if let index = dataModel.categories[categoryNum].contacts.firstIndex(of: contact) {
            let indexPath = IndexPath(row: index, section: 0)
            
            if let cell = tableView.cellForRow(at: indexPath) {
                configureText(for: cell, with: contact)
            }
        }
        
        dataModel.savePhonebook()
        navigationController?.popViewController(animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == K.addContactIdentifier {
            
            let controller = segue.destination as! ContactDetailVC
            controller.delegate = self
            
        } else if segue.identifier == K.editContactIdentifier {
            
            let controller = segue.destination as! ContactDetailVC
            controller.delegate = self
            
            if let indexPath = tableView.indexPath(for: sender as! UITableViewCell) {
                controller.contactToEdit = dataModel.categories[categoryNum].contacts[indexPath.row]
            }
        }
    }

}
